from gridworld import GridWorld1
import gridrender as gui
import numpy as np
import time
import matplotlib.pyplot as plt
from random import random

env = GridWorld1

################################################################################
# investigate the structure of the environment
# - env.n_states: the number of states
# - env.state2coord: converts state number to coordinates (row, col)
# - env.coord2state: converts coordinates (row, col) into state number
# - env.action_names: converts action number [0,3] into a named action
# - env.state_actions: for each state stores the action availables
#   For example
#       print(env.state_actions[4]) -> [1,3]
#       print(env.action_names[env.state_actions[4]]) -> ['down' 'up']
# - env.gamma: discount factor
################################################################################
# print(env.state2coord)
print('\nThe grid of states nb#')
print(env.coord2state)
# print(env.state_actions)
print('\nThe nb# of actions')
print(env.action_names)
for i, el in enumerate(env.state_actions):
    print("s{}: {}".format(i, env.action_names[el]))


################################################################################
# Policy definition
# If you want to represent deterministic action you can just use the number of
# the action. Recall that in the terminal states only action 0 (right) is
# defined.
# In this case, you can use gui.renderpol to visualize the policy
################################################################################
def ex_render_policy():
    """ An example

    :return: None
    """
    pol = [1, 2, 0, 0, 1, 1, 0, 0, 0, 0, 3]
    print('Gui.render')
    gui.render_policy(env, pol)


################################################################################
# Try to simulate a trajectory
# you can use env.render=True to visualize the transition
################################################################################


def first_sim():
    """ A first simulation

    :return:
    """
    env.render = True
    state = 0
    fps = 1
    for _ in range(5):
        action = np.random.choice(env.state_actions[state])
        nexts, reward, term = env.step(state, action)
        state = nexts
        time.sleep(1. / fps)


################################################################################
# You can also visualize the q-function using render_q
################################################################################
# first get the maximum number of actions available
def ex_renderq():
    max_act = max(map(len, env.state_actions))
    q = np.random.rand(env.n_states, max_act)
    gui.render_q(env, q)


################################################################################
# Work to do: Q4
################################################################################
# here the v-function and q-function to be used for question 4

def compute_j(v):
    """ A computation of J for a given value

    :param v: The value
    :return: J
    """
    v = np.array(v)
    return np.sqrt(np.sum(v ** 2 * mu0))


# Computing mu_0
mu0 = np.zeros(env.n_states)
for _ in range(1000):
    st = env.reset()
    mu0[st] += 1
mu0 = mu0 / 1000

v_q4 = np.array(
    [0.87691855, 0.92820033, 0.98817903, 0.00000000, 0.67106071, -0.99447514, 0.00000000, -0.82847001, -0.87691855,
     -0.93358351, -0.99447514])
j_q4 = compute_j(v_q4)


def compute_val_mc(n, t_max, gamma, pi=None, graph=True, render=False):
    """ Compute the Monte Carlo estimator of the Value of the policy

    :param n: Number of iterations
    :param t_max: Maximum duration of an iteration
    :param gamma: The decreasing parameter
    :param pi: the policy to evaluate
    :param graph: Boolean if we plot the graph
    :param render: Boolean if we render the iterations
    :return: The estimated value V_n
    """

    if pi is None:
        # If no given policy, it is the "right or up"
        pi = [(0 not in env.state_actions[state]) * 3 for state in range(env.n_states)]

    # Initialisation of the value
    value = np.zeros(env.n_states)
    nb_iter_state = np.zeros(env.n_states)
    diff = np.zeros(n)

    env.render = render
    fps = 30

    for k in range(n):
        # For each iteration, we reset the random walk
        init_state = env.reset()
        nb_iter_state[init_state] += 1
        actual_state = init_state

        for t in range(t_max):
            #  While not longer than Tmax, we walk
            action = pi[actual_state]

            next_s, reward, term = env.step(actual_state, action)
            actual_state = next_s

            value[init_state] += gamma ** t * reward

            if render:
                time.sleep(1. / fps)
            if term:
                # If terminal state, break
                break

        #  We compute the value, and the J
        v_n = value / (nb_iter_state + (nb_iter_state == 0))
        j_mc = compute_j(v_n)

        diff[k] = np.abs(j_mc - j_q4)

    if graph:
        plt.plot(diff)
        plt.axis('auto')
        plt.xlabel('n : number of simulated trajectories')
        plt.ylabel('||$J_n - J^\pi$||')
        plt.title('Results of the Monte Carlo estimator')

    nb_iter_state += (nb_iter_state == 0)  # This line makes sure we do not divide by 0
    return value / nb_iter_state


################################################################################
# Work to do: Q5
################################################################################
v_opt = [0.87691855, 0.92820033, 0.98817903, 0.00000000, 0.82369294, 0.92820033,
         0.00000000, 0.77818504, 0.82369294, 0.87691855, 0.82847001]


def get_greedy_expl_action(q, actual_state, epsilon=0.5):
    """ Compute the next action with the greedy exploration policy

    :param q: The Q-matrix
    :param actual_state:
    :param epsilon: The probability to get a random next action
    :return: The next action
    """
    if random() < epsilon:
        # With probability eps, we get a random action
        action = np.random.choice(env.state_actions[actual_state])
    else:
        # With probability 1-eps, we get the best action w.r.t. Q
        poss_action_act_state = [a for a in env.state_actions[actual_state]]
        ind_best_action = int(np.argmax([q[actual_state, a] for a in poss_action_act_state]))
        action = poss_action_act_state[ind_best_action]

    return action


def greedy_expl_policy(q, tau=1):
    """ Compute the Greedy Policy from a q-matrix

    :param q: the Q-matrix
    :param tau: The parameter
    :return: pi : the greedy policy
    """
    range_actions = range(env.action_names.shape[0])
    pi = [[[np.exp(q[state, action] / tau) for action in range_actions]] /
          np.sum([np.exp(q[state, action] / tau) for action in range_actions])
          for state in env.n_states]

    return pi


def greedy_policy(q):
    """ Compute the greedy policy from Q

    :param q:
    :return:
    """
    pi = [env.state_actions[state][
              np.argmax([q[state, action] for action in env.state_actions[state]])]
          for state in range(env.n_states)]

    return np.array(pi, int)


def q_learning(n, t_max, gamma=0.95, epsilon=0.5, graph=True):
    """ The Q-learning algorithm

    :param n: number of iterations
    :param t_max: the maximum duration of an iteration
    :param gamma: the decreasing parameter
    :param epsilon: the probability for the greedy exploration
    :param graph: Boolean if we plot the graph
    :return: The optimal policy
    """
    q = np.zeros((env.n_states, env.action_names.shape[0]))
    number_iter = np.zeros((env.n_states, env.action_names.shape[0]))

    diff = []

    for _ in range(n):
        # For each iteration, we reset the environment
        actual_s = env.reset()

        for t in range(t_max):
            # We explore
            action = get_greedy_expl_action(q, actual_s, epsilon)
            next_s, reward, term = env.step(actual_s, action)

            number_iter[actual_s, action] += 1
            alpha_t = 1 / number_iter[actual_s, action]

            q[actual_s, action] = \
                (1 - alpha_t) * q[actual_s, action] + alpha_t * \
                (reward + gamma * np.max(
                    [q[next_s, a] for a in env.state_actions[actual_s]]))

            actual_s = next_s

            if term:
                break

        pi_n = greedy_policy(q)
        v_pi_n = compute_val_mc(400, 200, 0.95, pi_n, False, False)

        diff.append(np.sqrt(np.sum((v_pi_n - v_opt) ** 2)))

    pi_n = greedy_policy(q)
    if graph:
        plt.plot(diff)
        plt.axis('auto')
        plt.xlabel('n : number of simulated trajectories')
        plt.ylabel('||$V_n - V^\pi$||')
        plt.title('Results of the Q-learning algorithm')

    return pi_n
