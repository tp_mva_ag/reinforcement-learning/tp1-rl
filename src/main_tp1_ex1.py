import numpy as np
import random
import matplotlib.pyplot as plt
from time import time

states = np.array([0, 1, 2])  # States list

actions = np.array([0, 1, 2])
action_names = np.array(['a0', 'a1', 'a2'])
state_action_rewards = np.array([[0, 0, 5 / 100],
                                 [0, 0, 0],
                                 [0, 0, 9 / 10]])

state_action_transition_probability = \
    np.array([[[0.55, 0.45, 0], [0.3, 0.7, 0], [1, 0, 0]],
              [[1, 0, 0], [0, 0.4, 0.6], [0, 1, 0]],
              [[0, 1, 0], [0, 0.6, 0.4], [0, 0, 1]]])


# s_a_t_p[actual_st][action][next_state] = probability to get to the next state


def trans_prob(actual_state, action, next_state):
    """Return the transition probability from a state to the next, given an action"""
    return state_action_transition_probability[actual_state, action, next_state]


def get_random_next_state(actual_state, action):
    """Returns a random next state based on transition probabilities"""
    rand = random.random()
    proba = 0
    for next_state in states:
        proba += trans_prob(actual_state, action, next_state)
        if rand <= proba:
            return next_state
    return -1


def reward_prob(actual_state, action):
    """Return the reward of an action

    :param actual_state:
    :param action: chosen action
    :return: reward:
    """
    return state_action_rewards[actual_state][action]


def compute_value(pi, gamma=0.95, avail_states=states):
    """Computes the value of the policy, using the direct computation formula

    :param pi: policy to evaluate
    :param gamma: discount factor
    :return: value:
    """
    transition_matrix = np.array(
        [[trans_prob(actual_state, pi[actual_state], next_state)
          for next_state in avail_states]
         for actual_state in avail_states])

    reward_vector = np.array([reward_prob(actual_state, pi[actual_state]) for actual_state in avail_states])
    reward_vector = np.transpose(reward_vector)

    value = (np.linalg.inv(np.eye(pi.shape[0]) - gamma * transition_matrix)).dot(reward_vector)
    return np.transpose(value)


def bellman_operator(v, gamma=0.95):
    """Computes the bellman operators of the V

    :param v: The argument of the operator
    :param gamma: The discount factor
    :return: bellman_V :
    """

    bellman_v = np.zeros(v.shape)
    for state in states:
        bellman_v[state] = np.max(
            [reward_prob(state, action)
             + gamma * np.sum(
                [trans_prob(state, action, next_state) * v[next_state]
                 for next_state in states])
             for action in actions])

    return bellman_v


def greedy_policy(v, gamma=0.95):
    """Compute the greedy policy

    :param v:
    :param gamma:
    :return: pi: greedy policy
    """

    pi = np.zeros(states.shape)
    for state in states:
        pi[state] = np.argmax(
            [reward_prob(state, action)
             + gamma * np.sum(
                [trans_prob(state, action, next_state) * v[next_state]
                 for next_state in states])
             for action in actions])

    return pi.astype(int)


def value_iteration(epsilon=0.01, gamma=0.95, plot_bool=True):
    list_v = np.array([[random.random(), random.random(), random.random()]])

    # print('Début algorithme VI')
    start_vi = time()
    nb_iter = 1

    list_v = np.vstack((list_v, bellman_operator(list_v[-1], gamma)))
    while np.max(list_v[-1] - list_v[-2]) > epsilon:
        list_v = np.vstack((list_v, bellman_operator(list_v[-1], gamma)))
        nb_iter += 1

    print('Value iteration: duration %6.5f sec | %s iterations' %
          (time() - start_vi, nb_iter))

    # Plotting the evolution of the norm of the distance.
    # Hence, we assume that the last v_k ~= v*
    dist = [np.sqrt(np.sum((v - list_v[-1]) ** 2)) for v in list_v]
    plt.plot(dist)
    plt.axis('auto')
    plt.xlabel('Iteration k')
    plt.ylabel('||$v_k - v^*$||')
    plt.title('Results of the value iteration algorithm')

    return greedy_policy(list_v[-1], gamma)


def policy_iteration(pi0, gamma=0.95):
    list_v = np.array([compute_value(pi0)])

    # print('Début algorithme PI')
    start_vi = time()
    nb_iter = 1

    # Première itération
    pi = greedy_policy(list_v[-1], gamma)
    list_v = np.vstack((list_v, compute_value(pi, gamma)))

    while not np.array_equal(list_v[-1], list_v[-2]):
        pi = greedy_policy(list_v[-1], gamma)
        list_v = np.vstack((list_v, compute_value(pi, gamma)))

        nb_iter += 1

    print('Policy iteration: duration %6.5f sec | %s iterations' %
          (time() - start_vi, nb_iter))

    # Plotting the evolution of the norm of the distance.
    # Hence, we assume that the last v_k ~= v*
    dist = [np.sqrt(np.sum((v - list_v[-1]) ** 2)) for v in list_v]
    plt.plot(dist)
    plt.axis('auto')
    plt.xlabel('Iteration k')
    plt.ylabel('||$v_k - v^*$||')
    plt.title('Results of the policy iteration algorithm')

    return pi


# print(value_iteration(0.01, 0.95))
# pi0 = np.array([0, 0, 0])
# print(policy_iteration(pi0, 0.95))
